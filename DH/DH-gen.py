from math import gcd as bltin_gcd
import random
import Crypto.Util.number

def primRoots(modulo):
    required_set = {num for num in range(1, modulo) if bltin_gcd(num, modulo) }
    return [g for g in range(1, modulo) if required_set == {pow(g, powers, modulo)
            for powers in range(1, modulo)}]

def GenDHParams():
	bits = 8
	Privkey = random.randint(1,2**bits)
	p = Crypto.Util.number.getPrime(bits, randfunc=Crypto.Random.get_random_bytes)
	g = primRoots(p)[0]
	Pkey = (g ** Privkey) % p
	
	return Pkey, Privkey, p

print(GenDHParams())
